const fs = require('fs');
const csvPar = require('csv-parser');

const results = {};
const order = {};
fs.createReadStream('../csv-files/matches.csv')
    .pipe(csvPar())
    .on('data',(RunsBySeason) => {
        const year = RunsBySeason.season;
        const teamFirst = RunsBySeason.team1;
        const teamSecond = RunsBySeason.team2;

        if(!results[teamFirst]){
            results[teamFirst] = {};
        }

        if(!results[teamSecond]){
            results[teamSecond] = {};
        }

        if(!results[teamFirst][year]){
            results[teamFirst][year] = 0;
        }

        if(!results[teamSecond][year]){
            results[teamSecond[year]] = 0;
        }

        results[teamFirst][year] += 1;
        results[teamSecond][year] += 1;
    })
    .on('end', () => {
        Object.keys(results).sort().forEach((key) => {
           order[key] = results[key];
        });
     for(let key in order){
         for(let i=2008; i< 2018; i +=1 ){
             if(order[key][i] ===undefined){
                 order[key][i]=0;
                }
            }
        }
     fs.writeFile('../gamesBySeason.json', JSON.stringify(order), () => {});
    });
  
