fetch('./totalRuns.json')
.then(resp => resp.json())
.then((data) => {
  const key = Object.keys(data);
  const value = Object.values(data);
  Highcharts.chart('container1', {
    chart: {
      type: 'column',
    },
    title: {
      text: 'Total runs scored by teams over the history of IPL(2008-2017)',
    },
    xAxis: {
      type: 'category',
      labels: {
        rotation: -45,
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif',
        },
      },
      categories: key,
    },
    yAxis: {
      min: 0,
      title: {
        text: ' Total Runs',
      },
    },
    legend: {
      enabled: false,
    },
    tooltip: {
      pointFormat: 'Total Runs: <b>{point.y:f}.</b>',
    },
    series: [{
      name: 'IPL',
      data: value,
      dataLabels: {
        enabled: true,
        rotation: -90,
        color: '#FFFFFF',
        align: 'right',
        format: '{point.y:f}', // one decimal
        y: 10, // 10 pixels down from the top
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif',
        },
      },
    }],
  });
});
