const fs = require('fs');
const csvPar = require('csv-parser');

//deliveries.csv file
 const results = {};
 fs.createReadStream('../csv-files/deliveries.csv')
     .pipe(csvPar())
     .on('data', (deliveries) => {
     //results1.push(deliveries.total_runs)
        const team = deliveries.batting_team;
        const runs = parseInt(deliveries.total_runs, 0);
        if(!results[team]){
            results[team] = 0;
        }
        results[team] += runs;
     })
     .on('end', () => {
         fs.writeFile('../totalRuns.json', JSON.stringify(results), () => {});
   });