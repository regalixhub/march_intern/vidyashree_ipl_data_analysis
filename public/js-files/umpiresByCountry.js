const fs = require('fs');
const csvPar = require('csv-parser');

//deliveries.csv file
 const umpires = {};
 fs.createReadStream('../csv-files/umpires.csv')
     .pipe(csvPar())
     .on('data', (umpire) => {
     //results1.push(deliveries.total_runs)
        const name = umpire.Nationality;
        if(name !== 'India'){
            if(!umpires[name]){
                umpires[name] = 0;
            }
            umpires[name] += 1;
        }

     })
     .on('end', () => {
         fs.writeFile('../umpires.json', JSON.stringify(umpires), () => {});
   });