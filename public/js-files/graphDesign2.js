fetch('./RCBTopBatsman.json')
.then(response => response.json())
  .then((data) => {
    const value = Object.values(data);
    const key = Object.keys(data);
    const players = [];
    for (let i = 0; i < value.length; i += 1) {
      players[i] = value[i][0];
    }
    Highcharts.chart('container2', {
      chart: {
        type: 'column',
      },
      title: {
        text: 'Total runs scored by RCB Batsmans',
      },
      xAxis: {
        type: 'category',
        labels: {
          rotation: -45,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif',
          },
          categories: key,
        },
      },
      yAxis: {
        min: 0,
        title: {
          text: ' total runs',
        },
      },
      legend: {
        enabled: false,
      },
      tooltip: {
        pointFormat: 'Total Runs: <b>{point.y:f}.</b>',
      },
      series: [{
        name: key,
        data: value,
        dataLabels: {
          enabled: true,
          rotation: -90,
          align: 'right',
          format: '{point.y:f}',
          y: 10,
          style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif',
          },
        },
      }],
    });
  });
