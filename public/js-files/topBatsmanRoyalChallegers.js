const fs = require('fs');
const csvPar = require('csv-parser');

const topBatsman = {};
fs.createReadStream('../csv-files/deliveries.csv')
     .pipe(csvPar())
     .on('data', (batsman) => {
     //results1.push(deliveries.total_runs)
        const bman = batsman.batsman;
        const team = batsman.batting_team;
        const runs = parseInt(batsman.batsman_runs, 0);
        if(team === 'Royal Challengers Bangalore')
        {
            if(!topBatsman[bman]){
                topBatsman[bman] = 0;
            }
            topBatsman[bman] += runs;
        }
     })
     .on('end', () => {
         let sortPlayers = [];
         for(var name in topBatsman){
             sortPlayers.push([name, topBatsman[name]])
         }
         sortPlayers.sort((a,b) => b[1] - a[1]);
         const top15 = sortPlayers.slice(0, 15);
         fs.writeFile('../RCBTopBatsman.json', JSON.stringify(top15), () => {});
   });