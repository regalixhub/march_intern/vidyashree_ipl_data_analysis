fetch('./gamesBySeason.json')
.then(resp => resp.json())
.then((data) => {
  const year = [2008];
  for (let i = 0; year[i] < 2018; i += 1) {
    year.push(1 + year[i]);
  }
  const serarr = [];
  const key = Object.keys(data);
  for (let i = 0; i < key.length; i += 1) {
    serarr.push(
      {
        name: key[i],
        data: Object.values(data[key[i]]),
      },
    );
  }
  
  Highcharts.chart('container4', {
    chart: {
      type: 'column',
    },
    title: {
      text: 'Stacked chart of matches played by team over the season',
    },
    xAxis: {
      categories: year,
    },
    yAxis: {
      title: {
        text: 'No. of games played per season',
      },
    },
    plotOptions: {
      series: {
        stacking: 'normal',
      },
    },
    series: serarr,
  });
});
