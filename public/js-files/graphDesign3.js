fetch('./umpires.json')
.then(resp => resp.json())
.then((data) => {
  const key = Object.keys(data);
  const value = Object.values(data);
  Highcharts.chart('container3', {
    chart: {
      type: 'column',
    },
    title: {
      text: 'Number of Umpires by Country',
    },
    xAxis: {
      type: 'category',
      labels: {
        rotation: -45,
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif',
        },
      },
      categories: key,
    },
    yAxis: {
      min: 0,
      title: {
        text: 'No. of Umpires',
      },
    },
    legend: {
      enabled: false,
    },
    tooltip: {
      pointFormat: 'Number of Umpires: <b>{point.y:f}.</b>',
    },
    series: [{
      name: 'IPL',
      data: value,
      dataLabels: {
        enabled: true,
        rotation: -90,
        color: '#FFFFFF',
        align: 'right',
        format: '{point.y:f}', // one decimal
        y: 10, // 10 pixels down from the top
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif',
        },
      },
    }],
  });
});
